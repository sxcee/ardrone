import imutils
import cv2

# This method gets an OpenCV frame as an input and tracks a yellow ball inside it.
#
# Parameters:
# - frame: An OpenCV frame to search the ball in.
#
# Returns:
# - frame: A modified version of the frame, that this method worked on
# - ballX: The x position of the ball within the frame, or -1 if no ball was found
# - ballY: The y position of the ball within the frame, or -1 if no ball was found
def getBallInFrame(frame):
    # Set initial coordinates to not found
    ballX, ballY = -1, -1

    # Specify the upper and lower HSV bounds for the yellow of the ball
    yellowLower = (20, 100, 100)
    yellowUpper = (30, 255, 255)

    # Scale image (why?)
    # frame = imutils.resize(frame, width=600)

    # Convert image to HSV color format
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Find a mask of all the pixels that are had a color within the specified yellow range
    mask = cv2.inRange(hsv, yellowLower, yellowUpper)
    # Erode the mask in two iterations
    # This will help to erase some small noisy pixels and only leave large connected
    # areas of pixels
    mask = cv2.erode(mask, None, iterations=2)
    # Dilate the picture in two iterations
    # This will now enlargen the pixels that hasn't been eroded away
    mask = cv2.dilate(mask, None, iterations=2)

    # Find contours in the picture
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

    # If any contours have been found we assume of it is the ball
    if len(cnts) > 0:
        # Find the biggest area of all found contours (we assume it's the ball)
        c = max(cnts, key=cv2.contourArea)
        # Find the smallest circle that contains all ball pixels
        ((x, y), radius) = cv2.minEnclosingCircle(c)

        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

        # Store the center of the found ball circle
        ballX = x
        ballY = y
        # Draw debug output to the image
        cv2.circle(frame, (int(x), int(y)), int(max(radius, 6)), (0, 255, 255), 2)
        cv2.circle(frame, center, 5, (0, 0, 255), -1)

    return frame, ballX, ballY
