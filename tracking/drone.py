import tracking, ps_drone
import time, argparse

def startDrone(debug):
    drone = ps_drone.Drone()
    drone.printBlue("Connecting to the drone")

    drone.startup()
    drone.reset()
    drone.printBlue("Connection established")

    #Waiting for the drone to accept
    while drone.getBattery()[0] == -1:
        time.sleep(0.1)
    time.sleep(0.5)

    #setup the video and wait until the drone is done with the config
    drone.useDemoMode(True)
    drone.setConfigAllID()
    drone.sdVideo()
    drone.groundCam()

    CDC = drone.ConfigDataCount
    if debug:
        drone.printBlue("CDC = " + str(CDC))

    while CDC == drone.ConfigDataCount:
        time.sleep(0.0001)

    drone.startVideo()
    drone.showVideo()
    time.sleep(2)

    drone.printBlue("Battery: {}% {}".format(drone.getBattery()[0], drone.getBattery()[1]))
    IMC =    drone.VideoImageCount
    stop, ground, flying, track =   False, True, False, False
    time.sleep(3)

    # Main loop
    while not stop:
        while drone.VideoImageCount == IMC: time.sleep(0.01)
        IMC = drone.VideoImageCount

        # Check for key input to control the drone
        key = drone.getKey()
        if key == " ":
            if drone.NavData["demo"][0][2] and not drone.NavData["demo"][0][3]:
                drone.takeoff()
                flying = True
            else:
                drone.land()

        elif key == "h":	drone.hover()
        elif key == "w":	drone.moveForward()
        elif key == "s":	drone.moveBackward()
        elif key == "a":	drone.moveLeft()
        elif key == "d":	drone.moveRight()
        elif key == "\x1b[A":
            drone.moveUp(1.0)
            time.sleep(0.5)
            drone.hover()
        elif key == "\x1b[B":
            drone.moveDown(1.0)
        elif key == "c":
            ground = not ground
            drone.groundVideo(ground)
        elif key == "t":
            track = not track
        elif key == "x":    stop = True
        elif key != "":		stop = True

        # Track ball in camera
        x_dir, y_dir, ballFound = tracking.show(drone.getImage(), debug)
        # If tracking was enabled via its hotkey and the drone is flying
        # follow the tracked ball
        if track and flying:
            if ground:
                tracking.moveDroneGround(drone, x_dir, y_dir, ballFound)
            else:
                tracking.moveDroneFront(drone, x_dir, y_dir, ballFound)

    print "Batterie: {}% {}".format(drone.getBattery()[0], drone.getBattery()[1])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Let the drone fly manually and let it track a yellow ball.')
    parser.add_argument('--debug', dest='debug', default=False, action='store_true',
                        help='Activate debug mode to show logs.')

    args = parser.parse_args()

    startDrone(args.debug)
