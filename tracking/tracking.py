import cv2
import image_recognition as ir
import ps_drone
import math

x_width, y_height = None, None

# Find a yellow ball inside the specified frame, highlights it and return
# the flying direction for the drone and whether a ball was found outside the
# center cell of the grid, i.e. it should be followed
def show(image, debug):
    image = drawGrid(image)
    image, ballX, ballY = ir.getBallInFrame(image)

    if debug:
        print "Ball X = {} Ball Y = {}".format(ballX, ballY)

    x_dir, y_dir, ballFound = getFlyingDirection(ballX, ballY, debug)

    cv2.imshow("GroundCam", image)
    cv2.waitKey(1)

    return x_dir, y_dir, ballFound

# Draws the used navigation grid onto the specified image
def drawGrid(image):
    global x_width, y_height
    if not x_width:
        x_width = image.shape[1]
    if not y_height:
        y_height = image.shape[0]

    cv2.line(image, ((2 * x_width) / 3, 0), ((2 * x_width) / 3, y_height), (0,0,0)) # zweite drittel oben nach unten
    cv2.line(image, (x_width / 3, 0), (x_width / 3, y_height), (0,0,0)) # erstes drittel oben nach unten

    cv2.line(image, (0, y_height / 3), (x_width, y_height / 3), (0,0,0)) # erstes drittel links nach rechts
    cv2.line(image, (0, (2 * y_height) / 3), (x_width, (2 * y_height) / 3), (0,0,0)) # zweites drittel links nach rechts

    return image

# Given the coordinates of a ball in the camera picture it returns into which
# direction the drone must fly to follow it.
#
# Returns:
# - x_direction: -1, 0 or 1 depending on the direction it should fly to
# - y_direction: -1, 0 or 1 depending on the direction it should fly to
# - must_fly: true if one of the above is not 0, i.e. the drone must move to follow the ball
def getFlyingDirection(ballX, ballY, debug):
    if ballX == -1 and ballY == -1:
        return 0, 0, False

    x_direction = math.floor(ballX / (x_width / 3.0)) - 1
    y_direction = -(math.floor(ballY / (y_height / 3.0)) - 1)

    if debug:
        print "x_direction = {} / y_direction = {}".format(x_direction, y_direction)

    return x_direction, y_direction, x_direction != 0 or y_direction != 0

def moveDroneGround(drone, x_direction, y_direction, ballFound):
    if ballFound:
        drone.move(x_direction / 8.0, y_direction / 8.0, 0.0, 0.0)
    else:
        drone.hover()

def moveDroneFront(drone, x_direction, y_direction, ballFound):
    if ballFound:
        drone.move(x_direction / 8.0, 0.0, y_direction / 8.0, 0.0)
    else:
        drone.hover()
