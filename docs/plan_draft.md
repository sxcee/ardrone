# AP drone plan

- set up a connection with the drone
- get the streamed video image (eg frames)
- analyze the video frames with openCV ("make openCV work")
- recognize the object to follow (openCV template matching)
- recognize the movement of the followed object (compare the coordinates of the followed objects on two images)
- follow the object (configure controls to move the drone depending on the movement of the object)
