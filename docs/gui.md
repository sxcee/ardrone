### Keyboard controls

| Key           | Control       |
| ------------- |:-------------:|
| Space         | takeoff / (smooth) landing |
| h | hover      |
| w | fly forward |
| s | fly backward|
| a | fly left |
| d | fly right |
| arrow up | fly up |
| arrow down | fly down |
| c |toggle between front and ground camera|
| x | stop and land on the spot |
