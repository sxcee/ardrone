# Dokumentation ARDrone WS16

__Gruppenmitglieder:__

Name	| Mail Hochschule |	Mail extern
Tanja | |
Daniel | |
Daniel | |


## Zusammenfassung / Abstract
_Kurze Darstellung der wesentlichen Aspekte der Arbeit. Dem potentiellen Leser soll geholfen werden, schnell zu_
_entscheiden, ob die Arbeit für ihn interessant seine könnte. Da das Abstract auch die Ergebnisse der Arbeit darstellt, kann es erst zum Schluss geschrieben werden._


## Keywords
<Kommaseparierte Liste von Schlüsselbegriffen, unter denen diese Arbeit z.B. hier im Wiki gefunden werden soll.>


## Arbeitsplan
### Must Haves
- [x] API Getting Started (Alle)
- [x] OpenCV Getting Started (Alle)
- [x] Bilderkennung des Balls mittels OpenCV (Alle).
- [x] Rasterposition mittels OpenCV erkennen (Daniel Varela).
- [ ] API-Seitige Funktionierende Steuerung.
- [x] Drone per API fliegen lassen (Daniel Mauch).
- [ ] Statusinformationen der Drone auslesen.
- [x] Kamerabild der Drone abfangen (Daniel und Tanja).
- [ ] Abstand der Ballposition ermittlen.
- [ ] Automatisierte Navigierung abhängig Ballposition im Raster (Tanja).

### Nice to Haves
- Ball lokalisieren


## Einführung
<Kurze Betrachtung des aktuellen technischen/wissenschaftlichen Standes, Aufzeigen der Motivation für die vorliegende Arbeit.>


## Aufgabenanalyse
<Genauere Formulierung der Aufgabenstellung, Analyse der Randbedingungen, Definition eigener Abgrenzungen, ....>


## Lösungsansatz
<Vergleich möglicher Lösungsansätze, Begründung des gewählten Lösungsansatzes, weitere Implikationen des gewählten Ansatzes, ...>


## Design
<Wie sieht die Lösungsstruktur aus (mit Begründung). Die Designbeschreibung soll auch die Struktur für die folgende Implementierungsbeschreibung vorgeben.>


## Implementierung
<Wie wurde die Aufgabe konkret gelöst, Beschreibung der Implementierungsdetails. Quelltextzitate nur falls sie zum Verständnis der Lösung beitragen.>


## Resultate
<Beschreibung der erreichten Ergebnisse und aufgetretenen Probleme.>


## Diskussion
<Wie sind die Ergebnisse zu interpretieren, welche Fragestellungen ergeben sich daraus, wie sehen die Ergebnisse im Vergleich zu anderen Publikationen aus.>


## Fazit und Ausblick
<Ähnlich wie das Abstract, eher zukunftsorientiert in Bezug auf weitergehende Fragestellungen. Beurteilung der Aufgabenstellung und der erreichten Ergebnisse. Wo und wie ließe sich das jetzt gewonnene Wissen weiterverwenden.>


## Arbeitsbuch
### 27.02.17
- Python Api für AR Drone beschäftigt. inklusive Insallation und Doku lesen.
- Python Api wird nicht für Windows empfohlen, da einige Funktionen nicht vorhanden sind, daher Umstieg auf Linux per VM.
- Installation auf POSIX-Systeme hingegen relativ einfach.
- Probleme gab es bei der Python Version, da nur Python Version 2.x unterstützt wird (seitens API)
- Verbindung per Wifi-Hotspot auf die Drone.
- Leider nur möglich mit einem Gerät, da die Kommunikation nur mit der Drone über die erste vergebene IP des Hotspots der Drone erfolgt.
- Einfache Handhabung der Drone ist bei erfolgreicher Verbindung mit der Drone über die Tastatureingabe möglich.
- Die Akkus der Drone sind leider schnell verbraucht.
- Plan aufgestellt für die kommende Woche (siehe plan_draft)
- Erste Versuche OpenCV einzubinden.
- Durch die nicht vorhandene Spezifikation der OpenCV Version, sind die ersten Versuche recht umständlich und durch viel Trial and Error geprägt.
- Zusätzlich wurde eine Versionskontrolle für die Aufgabe, sowie wie die Dokumentation erstellt.

### 28.02.17
- Auf Linux und macOS müssen OpenCV neu gebaut werden.
- OpenCV 2 mit Python konnte auf macOS zum Laufen gebracht werden.
- Bilder über die Webcam konnten eingeschränkt aufgenommen werden. Dabei kommt es nach einigen Sekunden zu einem `Segementation Error`.
- Mittels OpenCV kann der gelbe Ball über die Webcam abgefangen und verfolgt werden.
- Weitere Probleme verschaffen die unterschiedlichen Versionen der Dronen, da diese von unterschiedliche APIs unterstützt werden.
- Arbeitsplan überlegt und festgelegt.
- Auf macOS werden Fehler geschmissen, die wohl an der zu verwendenden OpenCV Version hängen (siehe: https://sourceforge.net/p/ps-drone/discussion/general/thread/a1711cf3/)
- Auf Linux konnte das Kamerabild der ArDrone 2 aufgenommen werden.
- Mit dem Hinzufügen der Kameraaufnahme zur eigentlichen API, konnte die Aufnahmen auch mit dem manuellen Steuern empfangen werden.
- interessant ist noch der Punkt, dass bei machen Betriebssytemen (Linux) die Kameraumschaltung nicht funktionert.
- Ferner ist eine deutliche Verzögerung auf der VM unter Mac zu beobachten (Daniel's MacBook)

### 01.03.17
- Eine Zeile könnte die Verzögerung in den Bildaufnahmen der Drone verusachen. Dies muss noch genauer untersucht werden.
- Tanja hat ein neues Modell für das Erkennen der Ballposition gefunden, welches daraus besteht das Kamerabild in 9 Quadranten aufzuteilen.
- Das neue Navigationsmodell soll weiter erforscht, und implementiert werden. Als Hilfe soll die Dokumenatation der vorgruppe dienen.
- Mit Linux VMs kann der angegebene Videospeicher die Performanz der Videoaufnahme stark einschränken.
- Eine Hardware Acceleration kann die Videodarstellung deutlich verbessern.
- Architektur Überlegungen für das Erstellen von einer raster-basierten Navigation.
- Festlegung der Arbeitsaufteilung für die Raster-Erkennung des Balls.
- API Funktionen für die Videoaufnahmen der Drone untersuchen.
- Tanja hat sich um die Navigation zum jeweiligen Quadranten des Rasters gekümmert.
- Daniel M. hat sich bei der Umsetzung des Rasters, um die Darstellung des Rasters.
- Daniel V. hat die Erkennung des Quadranten im Raster implementiert.
- Zusamenführung der drei Aufgabenbereiche durch alle Mitglieder.
- Einarbeitung Wiki durch Tanja und Daniel V. ...
- Daniel M. kümmerte sich derweilen um Bugfixes bei der Zusammenführung.
- Testen der Frontkamera mit Ballerkennung.
- Hinzufügen einer State-Erkennung, um das Tracking erst im Flug zu starten.


## Referenzen
<Verwendete Publikationen, Materialien, ...
Bitte bei allen Materialien, auch bei eigenen (z.B. Fotos), immer die jeweilige Quelle angeben. In den Anhängen können sie dafür das Kommentarfeld verwenden.>


## Ressourcen
<Link auf gepacktes Archiv der Arbeitsumgebung/Quelltexte/verwendete Libraries.>
