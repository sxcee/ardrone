# Daily zur Nachvollziehbarkeit

## 27.02.17
- Python Api für AR Drone beschäftigt. inklusive Insallation und Doku lesen.
- Python Api wird nicht für Windows empfohlen, da einige Funktionen nicht vorhanden sind, daher Umstieg auf Linux per VM.
- Installation auf POSIX-Systeme hingegen relativ einfach.
- Probleme gab es bei der Python Version, da nur Python Version 2.x unterstützt wird (seitens API)
- Verbindung per Wifi-Hotspot auf die Drone.
- Leider nur möglich mit einem Gerät, da die Kommunikation nur mit der Drone über die erste vergebene IP des Hotspots der Drone erfolgt.
- Einfache Handhabung der Drone ist bei erfolgreicher Verbindung mit der Drone über die Tastatureingabe möglich.
- Die Akkus der Drone sind leider schnell verbraucht.
- Plan aufgestellt für die kommende Woche (siehe plan_draft)
- Erste Versuche OpenCV einzubinden.
- Durch die nicht vorhandene Spezifikation der OpenCV Version, sind die ersten Versuche recht umständlich und durch viel Trial and Error geprägt.
- Zusätzlich wurde eine Versionskontrolle für die Aufgabe, sowie wie die Dokumentation erstellt.

## 28.02.17
- OpenCV 2 mit Python konnte auf macOS zum Laufen gebracht werden.
- Bilder über die Webcam konnten eingeschränkt aufgenommen werden. Dabei kommt es nach einigen Sekunden zu einem `Segementation Error`.
- Auf Linux und macOS müssen OpenCV neu gebuildet werden.
- Mittels OpenCV kann der gelbe Ball über die Webcam abgefangen und verfolgt werden. 
- Weitere Probleme verschaffen die unterschiedlichen Dronen Versionen, da diese nur unterschiedliche API unterstützen.